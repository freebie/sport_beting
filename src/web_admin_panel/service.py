import hashlib
import random
import time
from math import ceil

from flask import Flask, render_template, request, make_response, redirect, send_from_directory
from flask_bootstrap import Bootstrap
from flask_mongoengine import MongoEngine

from .. import config

from .db_model.user import User

from ..tg_bot.db_model.user import User as TgUser


from ..core.db_model.core import Core

from ..core.db_model.text.text import Text
from ..core.db_model.text.language import Language
from ..core.db_model.message import Message


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = config.PROJECT_NAME

app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

app.config['MONGODB_SETTINGS'] = {
    'db': config.PROJECT_NAME,
    'alias':  'default'
}

Bootstrap(app=app)
MongoEngine(app=app)


@app.route('/', methods=['GET', 'POST'])
def index():
    result = make_response(redirect('login'))
    if auth_check():
        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        if not user:
            return None

        core: Core = Core.objects.first()
        if request.method == 'POST':
            ref_1 = request.form.get('ref_1')
            if ref_1:
                core.ref_1 = float(ref_1)
            else:
                error = 'Вы не ввели цену на реф 1 уровня!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            ref_2 = request.form.get('ref_2')
            if ref_2:
                core.ref_2 = float(ref_2)
            else:
                error = 'Вы не ввели цену на реф 2 уровня!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            ref_3 = request.form.get('ref_3')
            if ref_3:
                core.ref_3 = float(ref_3)
            else:
                error = 'Вы не ввели цену на реф 3 уровня!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            ref_4 = request.form.get('ref_4')
            if ref_4:
                core.ref_4 = float(ref_4)
            else:
                error = 'Вы не ввели цену на реф 4 уровня!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            ref_5 = request.form.get('ref_5')
            if ref_5:
                core.ref_5 = float(ref_5)
            else:
                error = 'Вы не ввели цену на реф 5 уровня!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            price_vip = request.form.get('price_vip')
            if price_vip:
                core.price_vip = float(price_vip)
            else:
                error = 'Вы не ввели цену для вип!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            price_premium = request.form.get('price_premium')
            if price_premium:
                core.price_premium = float(price_premium)
            else:
                error = 'Вы не ввели цену для премиум!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            link = request.form.get('link')
            if link:
                core.link = link
            else:
                error = 'Вы не ввели ссылку!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            group_id = request.form.get('group_id')
            if group_id:
                core.group_id = int(group_id)
            else:
                error = 'Вы не ввели ID группы!'
                result = make_response(render_template('index.html', core=core, error=error))
                return result

            core.save()

        result = render_template(template_name_or_list='index.html',
                                 core=core)

    return result


@app.route('/users', methods=['GET', 'POST'])
def users():
    result = make_response(redirect('login'))
    if auth_check():
        page = int(request.args.get('p', None)) if request.args.get('p', None) else 1

        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        if not user:
            return None

        if request.method == 'POST':
            pass
        else:
            end = page * 20
            start = end - 20
            users_len = TgUser.objects.count()

            p_all = ceil(users_len / 20) if ceil(users_len / 20) != 0 else 1

            if start > users_len:
                start = users_len

            if end > users_len:
                end = users_len

            tg_users = TgUser.objects[start:end]
            result = render_template(template_name_or_list='users.html', p=page, rows=tg_users, p_all=p_all,
                                     u_all=users_len)
    return result


@app.route('/user/edit/<string:user_id>', methods=['GET', 'POST'])
def edit_user(user_id):
    result = make_response(redirect('users'))

    if not user_id:
        return result

    target_user = TgUser.objects(id=user_id).first()

    if request.method == 'POST':
        balance = request.form.get('balance')
        if balance:
            target_user.balance = float(balance)
        else:
            error = 'Вы не ввели баланс!'
            result = make_response(render_template('user.html', error=error, user=target_user))
            return result

        status = request.form['status']
        if status:
            target_user.status = status
        else:
            error = 'Вы не ввели статус!'
            result = make_response(render_template('language.html', error=error, user=target_user))
            return result

        target_user.save()
        result = make_response(redirect('users'))
    else:
        if target_user:
            result = render_template(template_name_or_list='user.html',
                                     user=target_user)

    return result


@app.route('/user/find', methods=['POST'])
def find_user():
    result = make_response(redirect('/users'))

    user_id = request.form.get('user_id')
    try:
        target_user: TgUser = TgUser.objects(user_id=int(user_id)).first()

        if user_id:
            result = make_response(redirect('/user/edit/'+str(target_user.id)))
    except:
        pass

    return result


@app.route('/user/del/<string:user_id>', methods=['GET', 'POST'])
def del_user(user_id):
    result = make_response(redirect('users'))

    if not user_id:
        return result

    target_user: TgUser = TgUser.objects(id=user_id).first()
    target_user.delete()

    return result


# language adding and editing
@app.route('/languages', methods=['GET', 'POST'])
def languages_method():
    result = make_response(redirect('login'))
    if auth_check():
        page = int(request.args.get('p', None)) if request.args.get('p', None) else 1

        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        if not user:
            return None

        if request.method == 'POST':
            pass
        else:
            end = page * 20
            start = end - 20
            languages_len = Language.objects.count()

            p_all = ceil(languages_len / 20) if ceil(languages_len / 20) != 0 else 1

            if start > languages_len:
                start = languages_len

            if end > languages_len:
                end = languages_len

            languages = Language.objects[start:end]
            result = render_template(template_name_or_list='languages.html', p=page, rows=languages, p_all=p_all,
                                     u_all=languages_len)
    return result


@app.route('/language', methods=['GET', 'POST'])
def language_method():
    result = make_response(redirect('login'))
    if auth_check():
        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        if not user:
            return None

        if request.method == 'POST':
            new_language = Language()

            name = request.form['name']
            if name:
                new_language.name = name
            else:
                error = 'Вы не ввели название!'
                result = make_response(render_template('language.html', error=error))
                return result

            tag = request.form['tag']
            if tag:
                new_language.tag = tag
            else:
                error = 'Вы не ввели тег!'
                result = make_response(render_template('language.html', error=error))
                return result

            button_text = request.form['button_text']
            if tag:
                new_language.button_text = button_text
            else:
                error = 'Вы не ввели текст кнопки!'
                result = make_response(render_template('language.html', error=error))
                return result

            new_language.save()
            result = make_response(redirect('languages'))
        else:
            result = render_template(template_name_or_list='language.html')

    return result


@app.route('/language/edit/<string:language_id>', methods=['GET', 'POST'])
def edit_language(language_id):
    result = make_response(redirect('languages'))

    if not language_id:
        return result

    target_language = Language.objects(id=language_id).first()

    if request.method == 'POST':
        name = request.form['name']
        if name:
            target_language.name = name
        else:
            error = 'Вы не ввели название!'
            result = make_response(render_template('language.html', error=error))
            return result

        tag = request.form['tag']
        if tag:
            target_language.tag = tag
        else:
            error = 'Вы не ввели тег!'
            result = make_response(render_template('language.html', error=error))
            return result

        button_text = request.form['button_text']
        if tag:
            target_language.button_text = button_text
        else:
            error = 'Вы не ввели текст кнопки!'
            result = make_response(render_template('language.html', error=error))
            return result

        target_language.save()
        result = make_response(redirect('languages'))
    else:
        if target_language:
            result = render_template(template_name_or_list='language.html',
                                     target_language=target_language)

    return result


@app.route('/language/del/<string:language_id>')
def del_language(language_id):
    result = make_response(redirect('languages'))

    if not language_id:
        return result

    target_language = Language.objects(id=language_id).first()

    if target_language:
        target_language.delete()

    return result


# text adding and editing
@app.route('/texts', methods=['GET', 'POST'])
def texts_method():
    result = make_response(redirect('login'))
    if auth_check():
        page = int(request.args.get('p', None)) if request.args.get('p', None) else 1

        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        if not user:
            return None

        if request.method == 'POST':
            pass
        else:
            end = page * 20
            start = end - 20
            texts_len = Text.objects.count()

            p_all = ceil(texts_len / 20) if ceil(texts_len / 20) != 0 else 1

            if start > texts_len:
                start = texts_len

            if end > texts_len:
                end = texts_len

            texts = Text.objects[start:end]
            result = render_template(template_name_or_list='texts.html', p=page, rows=texts, p_all=p_all,
                                     u_all=texts_len)

    return result


@app.route('/text', methods=['GET', 'POST'])
def text_method():
    result = make_response(redirect('login'))
    if auth_check():
        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()
        languages = Language.objects

        if not user:
            return None

        if request.method == 'POST':
            new_text = Text()

            tag = request.form.get('tag')
            if tag:
                new_text.tag = tag
            else:
                error = 'Вы не ввели тег!'
                result = make_response(render_template('text.html', languages=languages, error=error))
                return result

            for language in languages:
                value = request.form.get(language.tag + '_value')
                if value:
                    new_text.values[language.tag] = value
                else:
                    error = 'Вы не ввели значение  для языка: ' + language.name + '!'
                    result = make_response(render_template('text.html', languages=languages, error=error))
                    return result
            new_text.save()
            result = make_response(redirect('texts'))
        else:
            result = render_template(template_name_or_list='text.html', languages=languages)

    return result


@app.route('/text/edit/<string:text_id>', methods=['GET', 'POST'])
def edit_text(text_id):
    result = make_response(redirect('languages'))

    if not text_id:
        return result

    languages = Language.objects

    target_text = Text.objects(id=text_id).first()

    if not target_text:
        return None

    if request.method == 'POST':
        tag = request.form.get('tag')
        if tag:
            target_text.tag = tag
        else:
            error = 'Вы не ввели тег!'
            result = make_response(render_template('text.html', target_text=target_text, languages=languages,
                                                   error=error))
            return result

        for language in languages:
            value = request.form.get(language.tag + '_value')
            if value:
                target_text.values[language.tag] = value
            else:
                error = 'Вы не ввели значение  для языка: ' + language.name + '!'
                result = make_response(render_template('text.html', target_text=target_text, languages=languages,
                                                       error=error))
                return result
        target_text.save()
        result = make_response(redirect('texts'))
    else:
        result = render_template(template_name_or_list='text.html',
                                 target_text=target_text,
                                 languages=languages)

    return result


@app.route('/text/del/<string:text_id>', methods=['GET', 'POST'])
def del_text(text_id):
    result = make_response(redirect('/'))

    if not text_id:
        return result

    target_text = Text.objects(id=text_id).first()

    if target_text:
        target_text.delete()
        result = make_response(redirect('texts'))

    return result


@app.route('/mailing', methods=['GET', 'POST'])
def mailing():
    result = make_response(redirect('login'))

    if auth_check():
        error = None
        if request.method == 'POST':
            core = Core.objects.first()
            if core:
                message = Message()
                message.status = request.form.get('status')
                message.text = request.form.get('message')
                message.save()

                core.update(push__messages=message)
                core.reload()

        tg_users = TgUser.objects
        result = render_template(template_name_or_list='mailing.html',
                                 users=tg_users,
                                 error=error)

    return result


@app.route('/login', methods=['GET', 'POST'])
def login():
    result = make_response(render_template('login.html'))

    if not User.objects().first():
        user: User = User()
        user.login = 'sport'
        user.password = hashlib.md5('qwerty12345'.encode('utf-8')).hexdigest()
        user.save()

    if request.method == 'POST':
        user_login = request.form['login']
        password = request.form['password']

        user = User.objects(login=user_login).first()

        if user and user.password == hashlib.md5(password.encode('utf-8')).hexdigest():
            hash_str = (user_login + password + str(random.randrange(111, 999))).encode('utf-8')
            hashed_pass = hashlib.md5(hash_str).hexdigest()
            session_id = hashed_pass
            time_alive = time.time() + (86400 * 7)

            user.session_id = session_id
            user.time_alive = time_alive
            user.save()

            result = make_response(redirect(''))
            result.set_cookie('session_id', session_id)
        else:
            error = 'Пользователь с таким логином/паролем не найден!'
            result = make_response(render_template('login.html', error=error))
    else:
        if auth_check():
            result = make_response(redirect(''))

    return result


@app.route('/logout')
def logout():
    result = make_response(redirect('login'))

    session_id = request.cookies.get('session_id', None)
    if session_id:
        result.set_cookie('session_id', 'deleted', expires=time.time()-100)

    return result


def auth_check():
    result = False

    session_id = request.cookies.get('session_id', None)
    if session_id:
        user = User.objects(session_id=session_id).first()
        if user:
            result = True

    return result
