from mongoengine import *


class User(Document):
    user_id = IntField(required=True, unique=True)
    username = StringField()

    first_name = StringField()
    last_name = StringField()

    state = StringField(max_length=50)
    user_lang = StringField(default='rus')

