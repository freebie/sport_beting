import threading
import time
import datetime
import base64
import requests
import smtplib
import imghdr
import os
import json
import re
# import pycurl
# import subprocess

from telebot import types
from mongoengine.queryset.visitor import Q


from . import keyboards
from .state_handler import StateHandler


from .db_model.user import User
from ..core.db_model.core import Core
from ..core.db_model.message import Message
from .. import config
from ..core.db_model.text.text import Text
# from glpi import GlpiTicket, Ticket, GlpiProfile


class BotStates(StateHandler):
    def __init__(self, bot):
        super(BotStates, self).__init__(bot)
        self._register_states([
            self._start_state,
            self.main_menu_state,
            # self.payment_type,


        ])

        self._running = True

        self._kill_scheduling_event = threading.Event()
        self._kill_mailing_event = threading.Event()

        mailing = threading.Thread(target=self._mailing)
        mailing.daemon = True
        mailing.start()

    def __del__(self):
        self._running = False
        self._kill_mailing_event.set()
        self._kill_scheduling_event.set()

    def _mailing(self):
        core: Core = Core.objects.first()

        if not core:
            core = Core()
            core.save()

        while self._running and not self._kill_mailing_event.is_set():
            if len(core.messages) != 0:
                message: Message = core.messages[0]
                try:
                    if message:
                        users = User.objects()

                        for user in users:
                            try:
                                self._bot.send_message(user.user_id, message.text, parse_mode='markdown')
                                time.sleep(0.5)
                            except Exception as e:
                                print(e)
                except Exception as e:
                    print(e)
                finally:
                    core.update(pop__messages=1)
                    core.reload()

            core.reload()
            time.sleep(5)

    def _start_state(self, message: types.Message, entry=False):
        user: User = User.objects(user_id=message.chat.id).first()
        # self.user_clear(user.user_id)
        # user.reload()
        self._go_to_state(message, 'main_menu_state')

    def main_menu_state(self, message, entry=False):
        user: User = User.objects(user_id=message.chat.id).first()
        user_lang = user.user_lang

        if entry:
            self._bot.send_message(user.user_id, 'Hello, I know all about you!!')

    # def payment_type(self, message, entry=False, call: types.CallbackQuery = None):
    #     user: User = User.objects(user_id=message.chat.id).first()
    #     user_lang = user.user_lang
    #
    #     if entry:
    #         text = self.locale_text(user_lang, 'payment_type')
    #         self._bot.send_message(user.user_id, text, reply_markup=keyboards.payment_type(user_lang))
    #         print(1)




    @staticmethod
    def locale_text(lang, tag):
        if not lang or not tag:
            return None

        text = Text.objects(tag=tag).first()

        if not text:
            return None

        return text.values.get(lang)

    @staticmethod
    def user_clear(user_id):
        pass
