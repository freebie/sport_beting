from telebot import types

from ..core.db_model.text.text import Text


def get_lang_keyboard(buttons_text):
    lang_kb = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)

    for text in buttons_text:
        lang_kb.add(text)

    return lang_kb


def locale_text(lang, tag):
    if not lang or not tag:
        return None

    text = Text.objects(tag=tag).first()

    if not text:
        return None

    return text.values.get(lang)
