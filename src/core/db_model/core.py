from mongoengine import *

from .message import Message


class Core(Document):
    random = BooleanField(db_field='random', default=True)
    messages = ListField(db_field='messages', field=ReferenceField(Message), default=list())
